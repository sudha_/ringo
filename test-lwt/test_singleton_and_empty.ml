(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let ( let* ) = Lwt.bind

module H : Hashtbl.HashedType with type t = int = struct
   type t = int
   let equal = (=)
   let hash = Hashtbl.hash
end

module type EM_LIKE = module type of Ringo_lwt.Functors.Make_presized(Ringo.EmptyMap(H))
let test_em (module EM : EM_LIKE) =
   let c = EM.create () in
   let () = assert (EM.length c = 0) in
   let () = assert (EM.capacity c = 0) in
   let () = assert (EM.find_opt c 0 = None) in
   (* NOTE: even for empty-map, the value is returned, it's just not cached *)
   let* replaced = EM.find_or_replace c 0 (fun _ -> Lwt.return "zero") in
   let () = assert (replaced = "zero") in
   let () = assert (EM.length c = 0) in
   let () = assert (EM.find_opt c 0 = None) in
   Lwt.return ()
let () =
   Lwt_main.run
   @@ test_em (module Ringo_lwt.Functors.Make_presized(Ringo.EmptyMap(H)))

module type SM_LIKE = sig
   include Ringo_lwt.Sigs.CACHE_MAP with type key = H.t
   val create : unit -> 'a t
end
let test_sm (module SM : SM_LIKE) =
   let c = SM.create () in
   let () = assert (SM.length c = 0) in
   let () = assert (SM.capacity c = 1) in
   let () = assert (SM.find_opt c 0 = None) in
   let* replaced = SM.find_or_replace c 0 (fun _ -> Lwt.return "zero") in
   let () = assert (replaced = "zero") in
   let () = assert (SM.length c = 1) in
   let* ()  =
      match SM.find_opt c 0 with
      | None -> assert false
      | Some find ->
         let* find = find in
         assert (find = "zero");
         Lwt.return ()
   in
   let* found = SM.find_or_replace c 0 (fun _ -> assert false) in
   let () = assert (found = "zero") in
   let* replaced = SM.find_or_replace c 1 (fun _ -> Lwt.return "one") in
   let () = assert (replaced = "one") in
   let () = assert (SM.length c = 1) in
   let* ()  =
      match SM.find_opt c 1 with
      | None -> assert false
      | Some find ->
         let* find = find in
         assert (find = "one");
         Lwt.return ()
   in
   let () = assert (SM.find_opt c 0 = None) in
   Lwt.return ()
let () =
   Lwt_main.run
   @@ test_sm (module Ringo_lwt.Functors.Make_presized(Ringo.SingletonMap(H)))
let test_m ~replacement ~overflow ~accounting =
   let module M = struct
      include (val Ringo.map_maker ~replacement ~overflow ~accounting) (H)
      let create () = create 1
  end in
  Lwt_main.run @@ test_sm (module Ringo_lwt.Functors.Make_presized(M))
let () = test_m ~replacement:LRU ~overflow:Strong ~accounting:Sloppy
let () = test_m ~replacement:LRU ~overflow:Strong ~accounting:Precise
let () = test_m ~replacement:LRU ~overflow:Weak ~accounting:Sloppy
let () = test_m ~replacement:LRU ~overflow:Weak ~accounting:Precise
let () = test_m ~replacement:FIFO ~overflow:Strong ~accounting:Sloppy
let () = test_m ~replacement:FIFO ~overflow:Weak ~accounting:Sloppy
let () = test_m ~replacement:FIFO ~overflow:Strong ~accounting:Precise
let () = test_m ~replacement:FIFO ~overflow:Weak ~accounting:Precise

module type EMR_LIKE = module type of Ringo_lwt.Functors.Make_result_presized(Ringo.EmptyMap(H))
let test_em (module EMR : EMR_LIKE) =
   let c = EMR.create () in
   let () = assert (EMR.length c = 0) in
   let () = assert (EMR.capacity c = 0) in
   let () = assert (EMR.find_opt c 0 = None) in
   let* replaced = EMR.find_or_replace c 0 (fun _ -> Lwt.return_ok "zero") in
   let () = assert (replaced = Ok "zero") in
   let* replaced = EMR.find_or_replace c 0 (fun _ -> Lwt.return_error []) in
   let () = assert (replaced = Error []) in
   let () = assert (EMR.length c = 0) in
   let () = assert (EMR.find_opt c 0 = None) in
   Lwt.return ()
let () =
   Lwt_main.run
   @@ test_em (module Ringo_lwt.Functors.Make_result_presized(Ringo.EmptyMap(H)))

module type SMR_LIKE = sig
   include Ringo_lwt.Sigs.CACHE_MAP_RESULT with type key = H.t
   val create : unit -> ('a, 'err) t
end
let test_sm (module SMR : SMR_LIKE) =
   let c = SMR.create () in
   let () = assert (SMR.length c = 0) in
   let () = assert (SMR.capacity c = 1) in
   let () = assert (SMR.find_opt c 0 = None) in
   let* replaced = SMR.find_or_replace c 0 (fun _ -> Lwt.return_ok "zero") in
   let () = assert (replaced = Ok "zero") in
   let () = assert (SMR.length c = 1) in
   let* ()  =
      match SMR.find_opt c 0 with
      | None -> assert false
      | Some find ->
         let* find = find in
         assert (find = Ok "zero");
         Lwt.return ()
   in
   let* found = SMR.find_or_replace c 0 (fun _ -> assert false) in
   let () = assert (found = Ok "zero") in
   let* replaced = SMR.find_or_replace c 1 (fun _ -> Lwt.return_error []) in
   let () = assert (replaced = Error []) in
   let () = assert (SMR.length c = 1) in
   let () = assert (SMR.find_opt c 1 = None) in
   let* ()  =
      match SMR.find_opt c 0 with
      | None -> assert false
      | Some find ->
         let* find = find in
         assert (find = Ok "zero");
         Lwt.return ()
   in
   Lwt.return ()
let () =
   Lwt_main.run
   @@ test_sm (module Ringo_lwt.Functors.Make_result_presized(Ringo.SingletonMap(H)))
let test_m ~replacement ~overflow ~accounting =
   let module M = struct
      include (val Ringo.map_maker ~replacement ~overflow ~accounting) (H)
      let create () = create 1
  end in
  Lwt_main.run @@ test_sm (module Ringo_lwt.Functors.Make_result_presized(M))
let () = test_m ~replacement:LRU ~overflow:Strong ~accounting:Sloppy
let () = test_m ~replacement:LRU ~overflow:Strong ~accounting:Precise
let () = test_m ~replacement:LRU ~overflow:Weak ~accounting:Sloppy
let () = test_m ~replacement:LRU ~overflow:Weak ~accounting:Precise
let () = test_m ~replacement:FIFO ~overflow:Strong ~accounting:Sloppy
let () = test_m ~replacement:FIFO ~overflow:Weak ~accounting:Sloppy
let () = test_m ~replacement:FIFO ~overflow:Strong ~accounting:Precise
let () = test_m ~replacement:FIFO ~overflow:Weak ~accounting:Precise

module type EMO_LIKE = module type of Ringo_lwt.Functors.Make_opt_presized(Ringo.EmptyMap(H))
let test_em (module EMO : EMO_LIKE) =
   let c = EMO.create () in
   let () = assert (EMO.length c = 0) in
   let () = assert (EMO.capacity c = 0) in
   let () = assert (EMO.find_opt c 0 = None) in
   let* replaced = EMO.find_or_replace c 0 (fun _ -> Lwt.return_some "zero") in
   let () = assert (replaced = Some "zero") in
   let* replaced = EMO.find_or_replace c 0 (fun _ -> Lwt.return_none) in
   let () = assert (replaced = None) in
   let () = assert (EMO.length c = 0) in
   let () = assert (EMO.find_opt c 0 = None) in
   Lwt.return ()
let () =
   Lwt_main.run
   @@ test_em (module Ringo_lwt.Functors.Make_opt_presized(Ringo.EmptyMap(H)))

module type SMO_LIKE = sig
   include Ringo_lwt.Sigs.CACHE_MAP_OPT with type key = H.t
   val create : unit -> 'a t
end
let test_sm (module SMO : SMO_LIKE) =
   let c = SMO.create () in
   let () = assert (SMO.length c = 0) in
   let () = assert (SMO.capacity c = 1) in
   let () = assert (SMO.find_opt c 0 = None) in
   let* replaced = SMO.find_or_replace c 0 (fun _ -> Lwt.return_some "zero") in
   let () = assert (replaced = Some "zero") in
   let () = assert (SMO.length c = 1) in
   let* ()  =
      match SMO.find_opt c 0 with
      | None -> assert false
      | Some find ->
         let* find = find in
         assert (find = Some "zero");
         Lwt.return ()
   in
   let* found = SMO.find_or_replace c 0 (fun _ -> assert false) in
   let () = assert (found = Some "zero") in
   let* replaced = SMO.find_or_replace c 1 (fun _ -> Lwt.return_none) in
   let () = assert (replaced = None) in
   let () = assert (SMO.length c = 1) in
   let () = assert (SMO.find_opt c 1 = None) in
   let* ()  =
      match SMO.find_opt c 0 with
      | None -> assert false
      | Some find ->
         let* find = find in
         assert (find = Some "zero");
         Lwt.return ()
   in
   Lwt.return ()
let () =
   Lwt_main.run
   @@ test_sm (module Ringo_lwt.Functors.Make_opt_presized(Ringo.SingletonMap(H)))
let test_m ~replacement ~overflow ~accounting =
   let module M = struct
      include (val Ringo.map_maker ~replacement ~overflow ~accounting) (H)
      let create () = create 1
  end in
  Lwt_main.run @@ test_sm (module Ringo_lwt.Functors.Make_opt_presized(M))
let () = test_m ~replacement:LRU ~overflow:Strong ~accounting:Sloppy
let () = test_m ~replacement:LRU ~overflow:Strong ~accounting:Precise
let () = test_m ~replacement:LRU ~overflow:Weak ~accounting:Sloppy
let () = test_m ~replacement:LRU ~overflow:Weak ~accounting:Precise
let () = test_m ~replacement:FIFO ~overflow:Strong ~accounting:Sloppy
let () = test_m ~replacement:FIFO ~overflow:Weak ~accounting:Sloppy
let () = test_m ~replacement:FIFO ~overflow:Strong ~accounting:Precise
let () = test_m ~replacement:FIFO ~overflow:Weak ~accounting:Precise
