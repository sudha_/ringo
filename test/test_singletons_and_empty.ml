(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module H : Hashtbl.HashedType with type t = int = struct
   type t = int
   let equal = (=)
   let hash = Hashtbl.hash
end

module type EM_LIKE = module type of Ringo.EmptyMap(H)
let test_em (module EM : EM_LIKE) =
   let c = EM.create () in
   let () = assert (EM.length c = 0) in
   let () = assert (EM.capacity c = 0) in
   let () = assert (EM.find_opt c 0 = None) in
   let () = EM.replace c 0 "zero" in
   let () = assert (EM.length c = 0) in
   let () = assert (EM.find_opt c 0 = None) in
   ()
let () = test_em (module Ringo.EmptyMap(H))

module type SM_LIKE = sig
   include Ringo.CACHE_MAP with type key = H.t
   val create : unit -> 'a t
end
let test_sm (module SM : SM_LIKE) =
   let c = SM.create () in
   let () = assert (SM.length c = 0) in
   let () = assert (SM.capacity c = 1) in
   let () = assert (SM.find_opt c 0 = None) in
   let () = SM.replace c 0 "zero" in
   let () = assert (SM.length c = 1) in
   let () = assert (SM.find_opt c 0 = Some "zero") in
   let () = SM.replace c 0 "ziro" in
   let () = assert (SM.length c = 1) in
   let () = assert (SM.find_opt c 0 = Some "ziro") in
   let () = SM.replace c 1 "one" in
   let () = assert (SM.length c = 1) in
   let () = assert (SM.find_opt c 1 = Some "one") in
   let () = assert (SM.find_opt c 0 = None) in
   ()
let () = test_sm (module Ringo.SingletonMap(H))
let test_m ~replacement ~overflow ~accounting =
   let module M = struct
      include (val Ringo.map_maker ~replacement ~overflow ~accounting) (H)
      let create () = create 1
  end in
  test_sm (module M)
let () = test_m ~replacement:LRU ~overflow:Strong ~accounting:Sloppy
let () = test_m ~replacement:LRU ~overflow:Strong ~accounting:Precise
let () = test_m ~replacement:LRU ~overflow:Weak ~accounting:Sloppy
let () = test_m ~replacement:LRU ~overflow:Weak ~accounting:Precise
let () = test_m ~replacement:FIFO ~overflow:Strong ~accounting:Sloppy
let () = test_m ~replacement:FIFO ~overflow:Weak ~accounting:Sloppy
let () = test_m ~replacement:FIFO ~overflow:Strong ~accounting:Precise
let () = test_m ~replacement:FIFO ~overflow:Weak ~accounting:Precise

module type ES_LIKE = sig
   include Ringo.CACHE_SET with type elt = H.t
   val create : t
end
let test_es (module ES : ES_LIKE) =
   let c = ES.create in
   let () = assert (ES.length c = 0) in
   let () = assert (ES.capacity c = 0) in
   let () = assert (not (ES.mem c 0)) in
   let () = ES.add c 0 in
   let () = assert (ES.length c = 0) in
   let () = assert (not (ES.mem c 0)) in
   ()
let () = test_es (module Ringo.EmptySet(H))

module type SS_LIKE = sig
   include Ringo.CACHE_SET with type elt = H.t
   val create : unit -> t
end
let test_ss (module SS : SS_LIKE) =
   let c = SS.create () in
   let () = assert (SS.length c = 0) in
   let () = assert (SS.capacity c = 1) in
   let () = assert (not (SS.mem c 0)) in
   let () = SS.add c 0 in
   let () = assert (SS.length c = 1) in
   let () = assert (SS.mem c 0) in
   let () = SS.add c 1 in
   let () = assert (SS.length c = 1) in
   let () = assert (SS.mem c 1) in
   let () = assert (not (SS.mem c 0)) in
   ()
let () = test_ss (module Ringo.SingletonSet(H))
let test_s ~replacement ~overflow ~accounting =
   let module M = struct
      include (val Ringo.set_maker ~replacement ~overflow ~accounting) (H)
      let create () = create 1
  end in
  test_ss (module M)
let () = test_s ~replacement:LRU ~overflow:Strong ~accounting:Sloppy
let () = test_s ~replacement:LRU ~overflow:Strong ~accounting:Precise
let () = test_s ~replacement:LRU ~overflow:Weak ~accounting:Sloppy
let () = test_s ~replacement:LRU ~overflow:Weak ~accounting:Precise
let () = test_s ~replacement:FIFO ~overflow:Strong ~accounting:Sloppy
let () = test_s ~replacement:FIFO ~overflow:Weak ~accounting:Sloppy
let () = test_s ~replacement:FIFO ~overflow:Strong ~accounting:Precise
let () = test_s ~replacement:FIFO ~overflow:Weak ~accounting:Precise
